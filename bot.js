const CommandoClient = require('./client'); // Chargement du client
const path = require('path'); //on ajoute la librairie path
const fs = require('fs')
const dotenv = require('dotenv')

const envConfig = dotenv.parse(fs.readFileSync('.env'))
for (const k in envConfig) {
    process.env[k] = envConfig[k]
}


const client = new CommandoClient({
    commandPrefix: "!", // Préfixe des commandes (ex: !help)
    owner: process.env.BOT_OWNER_ID, // ID de l'owner du bot, peut également être un tableau d'id pour plusieurs owners, ex: ['ID1', 'ID2']
    disableMentions: 'everyone' // Désactive, par sécurité, l'utilisation du everyone par le bot
    /* invite: lien d'invit du discord dédié au bot */
});

client.on("guildMemberAdd", (member) => {
  //  const channel = member.guild.channels.cache.find(channel => channel.id("779678500546019348"));
    const channel = member.guild.channels.cache.find(ch => ch.name === 'welcome');
    const message = `Hey ${member.user}`;
     if(!channel) return;
     
     //envoyer dans le channel Welcome
    const welcomegreet = {
        color: 0xFCC0D,
        title: `**Welcome !**`,
        description: "Description \n XXX !",
        thumbnail: {
            url: member.user.avatarURL(),
        },
        fields: [
            {
                name: "Field title",
                value: "XXX \n XXX",
                inline : false
            },
            {
                name: "Field title",
                value: "XXX",
                inline: false
            }
        ],
        timestamp: new Date(),
    };
    
    //envoyer en message privé
    const welcome = {
        color: 0xFCC0D,
        title: `**Welcome !**`,
        description: "Description \n XXX !",
        thumbnail: {
            url: member.user.avatarURL(),
        },
        timestamp: new Date(),
    };
    channel.send(message, {embed: welcomegreet});
    member.send({embed: welcome});
});  //Welcome Message

client.on("guildMemberRemove", (member) => {
    const channel = member.guild.channels.cache.find(ch => ch.name === 'general');
    const leave = `${member.user} has left...`
    channel.send(leave);
});  //Leave Message

client.registry
    .registerDefaultTypes()
    .registerGroups([
        ["divers", 'Divers'], //la première valeur correspond à la section 'group' de la commande et la 2e pour l'afficahge du nom du groupe dans l'aide par exemple
        ["ark" , "ARK"],
		["reactions" , "Reactions"],
        ["moderation" , "Moderation"],
        ])  
    .registerCommandsIn(path.join(__dirname, 'commands'));
;

fs.readdir('./events/', (err, files) => {
    if (err) return console.error(err);
    files.forEach((file) => {
        const eventFunction = require(`./events/${file}`);
        if (eventFunction.disabled) return;

        const event = eventFunction.event || file.split('.')[0];
        const emitter = (typeof eventFunction.emitter === 'string' ? client[eventFunction.emitter] : eventFunction.emitter) || client;
        const { once } = eventFunction;

        try {
            emitter[once ? 'once' : 'on'](event, (...args) => eventFunction.run(client, ...args));
        } catch (error) {
            console.error(error.stack);
        }
    });
});


client.login(process.env.BOT_TOKEN);