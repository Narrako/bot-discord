﻿const { Command } = require('discord.js-commando');

module.exports = class EmbedCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'reaction',
            memberName: 'reaction',
            group: 'moderation',
            description: 'Renvoie un message avec des réactions !',
            ownerOnly: false,
          //  clientPermissions: ['SEND_MESSAGES', 'EMBED_LINKS'], // le bot doit avoir la permission d'envoyer des messages
        });
    }

    async run(msg) {
        const Discord = require('discord.js');

        const embed = new Discord.MessageEmbed(); // création de l'embed

        const mdrRole = msg.guild.roles.cache.get("781673174827270175");
        const lolRole = msg.guild.roles.cache.get("781792260864999425");

        embed
            .setColor(`BLUE`)
            .setTitle(`Roles`)
			.setDescription("Cliquez sur une des reactions ci-dessous pour obtenir le rôle correspondant")
			.setTimestamp()
            .addField(`Les rôles disponibles:`,
                `😂 - ${mdrRole.toString()} \n 🙃 - ${lolRole.toString()} `,true);

		msg.say(embed).then(async msg => {
			await msg.react('😂');
			await msg.react('🙃');
		})
    }
};