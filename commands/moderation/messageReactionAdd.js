﻿module.exports.run = async (client, messageReaction, user) => {
    const message = messageReaction.message;
    const member = message.guild.members.cache.get(user.id);
    const emoji = messageReaction.emoji.name;
 //   const channel = message.guild.channel.find(c=>c.id==="774583784543879188");
    const mdrRole = message.guild.roles.cache.get("781673174827270175");
    const lolRole = message.guild.roles.cache.get("781792260864999425");

    if(member.user.bot)return;

    if(["😂","🙃"].includes(emoji)){
        switch(emoji) {
            case "😂":
                member.roles.add(mdrRole);
                message.channel.send(`Le rôle ${mdrRole.name} a été ajouté avec succès!`);
                break;
            case "🙃":
                member.roles.add(lolRole);
                message.channel.send(`Le rôle ${lolRole.name} a été ajouté avec succès!`);
                break;
        };
    };
};