const { Command } = require('discord.js-commando');

module.exports = class ReplyCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'ko',
            memberName: 'ko',
            group: 'ark',
            description: 'Montre comment mettre ko un dino',
            clientPermissions: ['SEND_MESSAGES'], // le bot doit avoir la permission d'envoyer des messages
            throttling: {
                usages: 1,
                duration: 10,
            },
            args: [
                {
                    key: 'level',
                    prompt: 'Quel est le niveau du dino ?',
                    type: 'integer',
                },
            ], 
        });
    }

    async run(msg, { level }) {
        
        var T = 350 + (21 * Number(level));
        var a = T/90;
        var b = T/75;
        var c = T/157.5;
        var d = T/50;
        var e = T/300;
        var f = T/221;
        var g = T/442;
        
        const Discord = require('discord.js');

        const embed = new Discord.MessageEmbed(); // création de l'embed

        embed
            .setColor(`RED`)
          //  .setTitle('KO : Carnotaure')
            .setTitle(`KO dino level ${level}`)

           // .setDescription('Dino niveau:')

            // Fields

            // Sur une ligne complète :
            .addField(`Bow (Tranq Arrow)`,`Ammo required: ${Math.ceil(a)}`)
            .addField(`Longbow (Tranq Arrow)`,`Ammo required: ${Math.ceil(b)}`)
            .addField(`Crossbow (Tranq Arrow)`,`Ammo required: ${Math.ceil(c)}`)
            .addField(`Compound Bow (Tranq Arrow))`,`Ammo required: ${Math.ceil(d)}`)
            .addField(`Harpoon Launcher (Tranq Spear Bolt)`,`Ammo required: ${Math.ceil(e)}`)
            .addField(`Tranquilizer Dart)`,`Ammo required: ${Math.ceil(f)}`)
            .addField(`Shocking Tranquilizer Dart)`,`Ammo required: ${Math.ceil(g)}`)
        ;
            
        msg.say(embed)
    }
};