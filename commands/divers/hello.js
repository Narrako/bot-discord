const { Command } = require('discord.js-commando');

module.exports = class HelloCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'hello', //le nom de la commande qu'il faut taper sur discord
            memberName: 'hello',  //nom de la commande qui sera utilisé dans le groupe. Mettre même chose que dans name
            group: 'divers',  //nom du groupe associé à la commande
            aliases: ['bonjour', 'hi'],  //(facultatif) permet de déclarer des alias à une commande
            description: 'Replies with a hello message.',  //contient une desription de la commande, utilisé par la commande d'aide du bot
            guildOnly: false,  //(facultatif) si true la commande ne fonctionnera pas en message privé
            throttling: { //(facultatif) permet d'activer la protection anti-flood
                usages: 2, //nombre de fois qu'on peut faire appel à la commande sur un temps donné
                duration: 10, //temps donné avant de pouvoir utiliser à nouveau la commande en seconde
            },
        });
    }

    async run(msg) {
        msg.say(`Bonjour, je suis ${this.client.user.tag} `);
    }
};